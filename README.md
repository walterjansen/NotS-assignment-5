# Load Balancer

Naam: Walter Jansen
Studentennummer: 529530

## About
Dit is een een Http load balancer, geschreven in C# op het .NET platform

## Content

Overzicht van de inhoud
1. [Architectuur](#architectuur-van-de-applicatie)
2. [Load balancing](#load-balancing)
3. [Health Monitoring implementatie](#health-monitoring-implementatie)
4. [Reflectie](#reflectie)

---

## Architectuur van de applicatie
Hieronder is een klasse diagram te vinden waar de Architectuur van de applicatie duidelijk in wordt.

![Class diagram MVC structure](ClassDiagramLoadbalancer.png)

## Load balancing
In deze applicatie zijn 2 algoritmes om de load te verdelen geimplementeerd. Dit zijn Round robin en Random. Hieronder staat meer informatie over deze algoritmes

### Round Robin
Bij round robin wordt iedere server 1 voor 1 afgegaan. Wanneer er een lijst van servers aanwezig is wordt de lijst van boven naar beneden afgewerkt. Wanneer de laatste uit de lijst is geweest, begint het algorimte weer bij de eerste in de lijst.

### Code voorbeeld:

```cs
private int _currentRoundRobinIndex = 0;
//~~ A whole lot of other lines of code ~~//
private string SetHost(List<String> list)
{
    List<string> hostList = new List<string>(list);

    if (RoundRobin)
    {
        while (true)
        {
            if (_currentRoundRobinIndex >= hostList.Count) _currentRoundRobinIndex = 0;

            string host = hostList[_currentRoundRobinIndex];
            _currentRoundRobinIndex++;
            _writeInput("Try host: " + host);
            if (CheckServerHealth(host)) return host;
            _writeInput(host + " is not available. Another server is being checked");
            hostList.Remove(host);
        }
    }
}

```

### Random
Zoals de naam al zegt. De lijst met servers die aanwezig wordt willekeurig doogelopen. Bij dit proces hou ik in de applicatie geen load bij. Hier kwam ik te laat achter waardoor ik het niet heb kunnen inplementeren.

Daarnaast moet ik de random verbeteren. Dit kan ik doen door per server bij te houden wat de load is om vervolgens de request naar een server te sturen met minder load. Wanneer de load bij meerdere servers gelijk is, dan zal de server willekeurig uitgekozen kunnen worden.

#### Code voorbeeld:
```cs
private string SetHost(List<String> list)
{
    List<string> hostList = new List<string>(list);
    if (RandomServer)
    {
        while (true)
        {
            int randomNumber = new Random().Next(0, hostList.Count);
            string host = hostList[randomNumber];
            _writeInput("Try host: " + host);
            if (CheckServerHealth(host)) return host;
            hostList.Remove(host);
        }
    }
}
```

### Alternatieven
Binnen Round Robin zijn er nog twee verschillende takken om toe te passen:
- Weighted Round Robin
- Dynamic Round Robin

#### Weighted Round Robin
Bij het toepassan van dit algoritme kan je bepalen welke server meer load aankan dan een andere server. Voorbeeld: server 1 en server 2 kunnen beide elke 10 request aan en server 3 kan 20 request aan. De load balancer zal nu 2 requests naar server 3 sturen en 1 request naar server 1 en server 2.
#### Dynamic Round Robin
Bij het toepassan van dit algoritme wordt dynamisch bepaald welke server, bijvoorbeeld, het snelste kan reageren. Zo kan bijvoorbeeld server 1 een paar keer sneller hebben gereageerd dan server 2, waardoor server 1 vaker gekozen zal worden.

Verder zijn er nog een aantal andere algoritmes, deze luiden als volgt:
- Fastest
- Least Connections
- Observed
- Predictive

In dit document zal alleen Fastest besproken worden
#### Fastest
Bij Fastest wordt de snelste server over de gehele tijd van applicatie tijd gekozen in voorkeur. Dit wordt vooral gebruikt verschillende servers zijn gedistribueerd in verschillende logische netwerken.

### Bronnen
[Intro to Load Balancing for Developers](https://devcentral.f5.com/articles/intro-to-load-balancing-for-developers-ndash-the-algorithms)



---

## Health Monitoring implementatie
Om zeker te zijn dat de laodbalancer niet met servers gaat verbinden die niet beschikbaar zijn, is er een health check gebouwd. In deze health check wordt een HEAD request naar de server gestuurd waarmee verbinding zal worden gemaakt.

### Code voorbeeld
Doormiddel van de WebRequest class wordt een request gemaakt. Vervolgens wordt de methode verandert, deze staat als default op GET. Vervolgens wordt er om een response gevraagd. Waneer de applicatie geen antwoord krijgt zal dit stuk falen. Om dit af te vangen zit er een try catch clause omheen gebouwd.
```cs
private bool CheckServerHealth(string host)
{
    try
    {
        WebRequest request = WebRequest.Create("http://" + host + "/");
        request.Method = "HEAD";
        request.GetResponse();
        return true;
    }
    catch {return false;}
}
```
### Alternatieven
Het is ook mogelijk om een nieuwe TcpClient aan te maken. Vervolgens een nieuwe stream aan te maken. Dan zal er naar de server geschreven worden en vervolgens uitlezen. Wanneer het schrijven of het lezen faalt is het dus niet mogelijk om te verbinden. Ik heb niet voor deze methode gekozen, omdat ik dan zelf mijn request headers moet maken en meer regels aan code heb. Door een simpel HEAD request te sturen wordt ook gekeken of de server beschikbaar is. Daarnaast vond ik het ook leerzaam om naar andere oplossingen te kijken dan een oplossing die al wordt gebruikt.

### Bronnen
[Stack overflow - How to request only the HTTP header with C#?]( http://stackoverflow.com/questions/6237734/how-to-request-only-the-http-header-with-c)

[MSDN - WebRequest](https://msdn.microsoft.com/en-us/library/system.net.webrequest.aspx)

## Reflectie

Ik ben blij met de performance van de applicatie. Tijdens het testen heb ik geen last gehad van een vastlopende applicatie.
De code kwaliteit wil ik echter wel verbeteren. Ik wil alles meer kunnen verdelen in klassen en betere namen gebruiken. Met name met oog op uitbreidbaarheid van de code en herbruikbaarheid. Daar heb ik de meeste gevallen wel zo goed mogelijk naar gekeken. 
