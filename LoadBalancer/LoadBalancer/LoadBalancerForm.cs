﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LoadBalancer.Delegates;

namespace LoadBalancer
{
    public partial class LoadBalancerForm : Form
    {
        private LoadBalancer _lb;
        private readonly ReadServerListDelegate _readServerListDelegate;

        public LoadBalancerForm()
        {
            InitializeComponent();
            _readServerListDelegate += GetServerList;

            // Sets de default for the drop down boxes to zero;
            persistanceList.SelectedIndex = 0;
            algoritmeList.SelectedIndex = 0;
        }

        /// <summary>
        /// Method for the input delegate
        /// </summary>
        /// <param name="input"></param>
        private void WriteInputToListMsg(string input)
        {
            if (logList.InvokeRequired)
            {
                WriteInputDelegate obj = WriteInputToListMsg;
                Invoke(obj, input);
            }
            else
            {
                logList.Items.Add(input);
                logList.TopIndex = logList.Items.Count - 1;
            }
        }

        private List<string> GetServerList()
        {
            List<String> serversList = serverList.Items.Cast<string>().ToList();
            return serversList;
        } 

        private void startStopBtn_Click(object sender, EventArgs e)
        {
            WriteInputToListMsg("Load balancer started...");
            _lb = new LoadBalancer(WriteInputToListMsg, _readServerListDelegate);
            _lb.StartListener((int)portNum.Value);
            algoritmeList.Enabled = true;
            persistanceList.Enabled = true;
            startStopBtn.Enabled = false;
        }

        private void serverList_Enter(object sender, EventArgs e)
        {
            this.deleteServerBtn.Enabled = true;
        }

        private void serverList_Leave(object sender, EventArgs e)
        {
//            this.deleteServerBtn.Enabled = false;
        }

        private void LoadBalancerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
//            _lb.StopListener();
        }

        private void persistanceList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_lb != null)
            {
                switch (persistanceList.Text)
                {
                    case "Session Based":
                        _lb.SessionBased = true;
                        _lb.CookieBased = false;
                        break;
                    case "Cookie Based":
                        _lb.SessionBased = false;
                        _lb.CookieBased = true;
                        break;
                }
            }
        }

        private void algoritmeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_lb != null)
            {
                switch (algoritmeList.Text)
                {
                    case "Round Robin":
                        _lb.RoundRobin = true;
                        _lb.RandomServer = false;
                        break;
                    case "Random":
                        _lb.RoundRobin = false;
                        _lb.RandomServer = true;
                        break;
                }
            }
        }

        private void addServerBtn_Click(object sender, EventArgs e)
        {
            if (serverTextBox.Text != "")
            {
                serverList.Items.Add(serverTextBox.Text);
                serverTextBox.Text = "";
            }
        }

        private void deleteServerBtn_Click(object sender, EventArgs e)
        {
            serverList.Items.Remove(serverList.SelectedItem);
        }
    }
}
