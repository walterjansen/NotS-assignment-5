﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoadBalancer
{
    public class ClientCookie
    {
        public string ClientId { get; set; }
        public string CookieNameValue { get; set; }
        public string RemoteHost { get; set; }

        public ClientCookie(string clientId, string cookieNameValue, string remoteHost)
        {
            ClientId = clientId;
            CookieNameValue = cookieNameValue;
            RemoteHost = remoteHost;
        }
    }
}
