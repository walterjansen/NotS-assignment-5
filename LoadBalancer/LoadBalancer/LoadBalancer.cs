﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using LoadBalancer.Delegates;

namespace LoadBalancer
{
    public class LoadBalancer
    {
        private TcpListener _listener;
        private Thread _listenerThread;
        private readonly WriteInputDelegate _writeInput;
        private readonly ReadServerListDelegate _readServer;

        private List<ClientCookie> clientCookies; 

        private int _currentRoundRobinIndex = 0;
        public bool RoundRobin { get; set; }
        public bool RandomServer { get; set; }
        public bool SessionBased { get; set; }
        public bool CookieBased { get; set; }


        public LoadBalancer(WriteInputDelegate writeInput, ReadServerListDelegate readServer)
        {
            _writeInput = writeInput;
            _writeInput("Created Load balancer");
            _readServer = readServer;
            clientCookies = new List<ClientCookie>();
            RoundRobin = false;
            RandomServer = true;
            SessionBased = true;
            CookieBased = false;
        }

        public void StartListener(int port)
        {
            _listener = new TcpListener(IPAddress.Parse("127.0.0.1"), port);
            _listener.Start();
            _listenerThread = new Thread(() =>
            {
                _writeInput("Started!");
                while (true)
                {
                    if (!_listener.Pending()) continue;
                    new Thread(() => HandleRequest(_listener.AcceptTcpClient())).Start();
                }
            });
            _listenerThread.Start();
        }

        private void HandleRequest(TcpClient tcpClient)
        {
            string EOL = "\r\n";

            byte[] requestBuffer = new byte[1];
            byte[] responseBuffer = new byte[1];
            NetworkStream stream = tcpClient.GetStream();
            ClientCookie clientCookie = null;

            try
            {
                //Handle Request from Client
                var requestLines = RecieveRequest(stream, requestBuffer);
                string remoteHost = "";

                //Filter every bullshit lines out of the request lines
                var requestPayload = "";
                foreach (string line in requestLines)
                {
                    if (line.Contains("User-Agent") || line.Contains("Upgrade-Insecure-Requests")) continue;

                    if (line.Contains("gzip") || line.Contains("Connection"))
                    {
                        requestPayload += "Connection: close";
                        requestPayload += EOL;
                        continue;
                    }

                    if (line.Contains("Cookie"))
                    {
                        if (line.Contains("connect.sid") && SessionBased)
                        {
                            foreach (var cookie in clientCookies)
                            {
                                if (line.Split(':')[1].Trim() == cookie.CookieNameValue)
                                {
                                    clientCookie = cookie;
                                    break;
                                }
                            }
                        }
                        else if (line.Contains("loadbalancer") && CookieBased)
                            remoteHost = line.Split('=')[1].Split(';')[0].Trim();
                    }

                    requestPayload += line;
                    requestPayload += EOL;
                    _writeInput(line);
                }

                
                bool cookieServerHostAvailable = true;

                //Select the right server, based on round robin or random 
                //(random is not quite the best way, but it's nice and it works)
                if ((clientCookie == null && SessionBased) || (remoteHost == "" && CookieBased))
                    remoteHost = SetHost(_readServer());
                else if (SessionBased)
                {
                    remoteHost = clientCookie.RemoteHost;
                    if (!CheckServerHealth(remoteHost.Split(':')[0]))
                    {
                        cookieServerHostAvailable = false;
                        string badReq =
                            "HTTP/1.1 200 OK\r\nX-Powered-By: Express\r\nContent-Type: text/html; charset=utf-8\r\nContent-Length: 206\r\nETag: W/\"6b-Flghmonra8V+gpfAuqsanQ\"\r\nConnection: close\r\n\r\n<!Doctype HTML>\n<html><head><title>MAGIC LOADBALANCER SPEAKIN'</title></head><body><h1>MAGIC LOADBALANCER SPEAKIN' </h1><p>you fucked up, shit broke down, keep trying bro</p><p>😂 rekt 😂</p></body></html>";
                        stream.Write(Encoding.ASCII.GetBytes(badReq), 0, Encoding.ASCII.GetBytes(badReq).Length);
                    }
                }

                if (cookieServerHostAvailable)
                {
                    //Fix remote host with a port
                    int remotePort = 80;
                    string originalHost = remoteHost;
                    if (remoteHost.Split(':')[1] != "")
                    {
                        remotePort = Int32.Parse(remoteHost.Split(':')[1]);
                        remoteHost = remoteHost.Split(':')[0];
                    }

                    //Write the request from the client to the remote host
                    Stream destStream = new TcpClient(remoteHost, remotePort).GetStream();
                    destStream.Write(Encoding.ASCII.GetBytes(requestPayload), 0,
                        Encoding.ASCII.GetBytes(requestPayload).Length);

                    //Recieve from the remote host and write to the client
                    string responsePayload = "";
                    string responseTempLine = "";
                    while (destStream.Read(responseBuffer, 0, responseBuffer.Length) != 0)
                    {
                        string fromByte = Encoding.ASCII.GetString(responseBuffer);
//                        responsePayload += fromByte;
                        responseTempLine += fromByte;

                        if (responseTempLine.EndsWith(EOL))
                        {
                            if (responseTempLine.Contains("connect.sid") && SessionBased)
                            {
                                ClientCookie tempCookie = new ClientCookie("localhoster",
                                        responseTempLine.Split(':')[1].Trim().Split(';')[0], originalHost);
                                if (clientCookie != null) clientCookies.Remove(clientCookie);
                                clientCookies.Add(tempCookie);
                            } else if (responseTempLine.Contains("connect.sid") && CookieBased)
                            {
                                responseTempLine = "set-cookie: loadbalancer=" + originalHost + "; Path=/; Expires=" + DateTime.Now.AddHours(3).ToString("R") + "; HttpOnly" + EOL;
                            }
                            requestLines.Add(responseTempLine.Trim());
                            responsePayload += responseTempLine;
                            responseTempLine = "";
                        }
//                        stream.Write(responseBuffer, 0, responseBuffer.Length);
                    }

                    stream.Write(Encoding.ASCII.GetBytes(responsePayload), 0, Encoding.ASCII.GetBytes(responsePayload).Length);

                    destStream.Close();
                    destStream.Dispose();

                    stream.Close();
                    stream.Dispose();
                }
            }
            catch (Exception e)
            {
                _writeInput("Error Occured: " + e.Message);
                Console.WriteLine("Error Occured: " + e.StackTrace);
            }
        }
        

        public void StopListener()
        {
            _listenerThread.Abort();
            _listener.Stop();
        }

        /// <summary>
        /// Recieves the request from the browser to the application
        /// </summary>
        /// <param name="stream">Stream from the connected TCP CLient</param>
        /// <param name="buffer">Request buffer byte array</param>
        /// <returns></returns>
        private List<string> RecieveRequest(Stream stream, Byte[] buffer)
        {
            string requestPayload = "";
            string requestTempLine = "";
            string EOL = "\r\n";
            List<string> requestLines = new List<string>();

            while (true)
            {
                stream.Read(buffer, 0, buffer.Length);
                string fromByte = Encoding.ASCII.GetString(buffer);
                requestPayload += fromByte;
                requestTempLine += fromByte;

                if (requestTempLine.EndsWith(EOL))
                {
                    requestLines.Add(requestTempLine.Trim());
                    requestTempLine = "";
                }

                if (requestPayload.EndsWith(EOL + EOL)) break;
            }
            return requestLines;
        }
        
        /// <summary>
        /// Host setter, needs to know if it is round robin or random server
        /// Doing that last thing, but method needs a cleanup
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private string SetHost(List<String> list)
        {
            List<string> hostList = new List<string>(list);

            if (RandomServer)
            {
                while (true)
                {
                    int randomNumber = new Random().Next(0, hostList.Count);
                    string host = hostList[randomNumber];
                    _writeInput("Try host: " + host);
                    if (CheckServerHealth(host)) return host;
                    _writeInput(host + " is not available. Another server is being checked");
                    hostList.Remove(host);
                }
            }
            if (RoundRobin)
            {
                while (true)
                {
                    if (_currentRoundRobinIndex >= hostList.Count) _currentRoundRobinIndex = 0;
                    
                    string host = hostList[_currentRoundRobinIndex];
                    _currentRoundRobinIndex++;
                    _writeInput("Try host: " + host);
                    if (CheckServerHealth(host)) return host;
                    _writeInput(host + " is not available. Another server is being checked");
                    hostList.Remove(host);
                }
            }
            return "";
        }

        private bool CheckServerHealth(string host)
        {
            try
            {
                WebRequest request = WebRequest.Create("http://" + host + "/");
                request.Method = "HEAD";
                request.GetResponse();
                return true;
            }
            catch {return false;}
        }
    }
}