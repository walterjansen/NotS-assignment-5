﻿namespace LoadBalancer
{
    partial class LoadBalancerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.logList = new System.Windows.Forms.ListBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.serverTextBox = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.serverList = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.portNum = new System.Windows.Forms.NumericUpDown();
            this.startStopBtn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.persistanceList = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.algoritmeList = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.deleteServerBtn = new System.Windows.Forms.Button();
            this.addServerBtn = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.portNum)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 185F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(736, 592);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.groupBox3, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 586F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(545, 586);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.logList);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(539, 580);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Log";
            // 
            // logList
            // 
            this.logList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logList.FormattingEnabled = true;
            this.logList.Location = new System.Drawing.Point(3, 16);
            this.logList.Name = "logList";
            this.logList.Size = new System.Drawing.Size(533, 561);
            this.logList.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.groupBox5, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.groupBox1, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.startStopBtn, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.groupBox2, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.groupBox4, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 0, 6);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(554, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 7;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(179, 586);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.serverTextBox, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 518);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(173, 27);
            this.tableLayoutPanel4.TabIndex = 7;
            // 
            // serverTextBox
            // 
            this.serverTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.serverTextBox.Location = new System.Drawing.Point(3, 3);
            this.serverTextBox.Name = "serverTextBox";
            this.serverTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.serverTextBox.Size = new System.Drawing.Size(167, 20);
            this.serverTextBox.TabIndex = 2;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.serverList);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(3, 193);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(173, 319);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Server List";
            // 
            // serverList
            // 
            this.serverList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.serverList.FormattingEnabled = true;
            this.serverList.Items.AddRange(new object[] {
            "node153.tezzt.nl:8081",
            "node153.tezzt.nl:8082",
            "node153.tezzt.nl:8083",
            "node153.tezzt.nl:8084"});
            this.serverList.Location = new System.Drawing.Point(3, 16);
            this.serverList.Name = "serverList";
            this.serverList.Size = new System.Drawing.Size(167, 300);
            this.serverList.TabIndex = 4;
            this.serverList.Enter += new System.EventHandler(this.serverList_Enter);
            this.serverList.Leave += new System.EventHandler(this.serverList_Leave);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.portNum);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 43);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(173, 44);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Loadbalancer port";
            // 
            // portNum
            // 
            this.portNum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.portNum.Location = new System.Drawing.Point(3, 16);
            this.portNum.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.portNum.Minimum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.portNum.Name = "portNum";
            this.portNum.Size = new System.Drawing.Size(167, 20);
            this.portNum.TabIndex = 0;
            this.portNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.portNum.Value = new decimal(new int[] {
            8080,
            0,
            0,
            0});
            // 
            // startStopBtn
            // 
            this.startStopBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.startStopBtn.Location = new System.Drawing.Point(3, 3);
            this.startStopBtn.Name = "startStopBtn";
            this.startStopBtn.Size = new System.Drawing.Size(173, 34);
            this.startStopBtn.TabIndex = 1;
            this.startStopBtn.Text = "Start / Stop";
            this.startStopBtn.UseVisualStyleBackColor = true;
            this.startStopBtn.Click += new System.EventHandler(this.startStopBtn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.persistanceList);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 93);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(173, 44);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Persistentie";
            // 
            // persistanceList
            // 
            this.persistanceList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.persistanceList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.persistanceList.Enabled = false;
            this.persistanceList.FormattingEnabled = true;
            this.persistanceList.Items.AddRange(new object[] {
            "Session Based",
            "Cookie Based"});
            this.persistanceList.Location = new System.Drawing.Point(3, 16);
            this.persistanceList.Name = "persistanceList";
            this.persistanceList.Size = new System.Drawing.Size(167, 21);
            this.persistanceList.TabIndex = 3;
            this.persistanceList.SelectedIndexChanged += new System.EventHandler(this.persistanceList_SelectedIndexChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.algoritmeList);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(3, 143);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(173, 44);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Algoritme";
            // 
            // algoritmeList
            // 
            this.algoritmeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.algoritmeList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.algoritmeList.Enabled = false;
            this.algoritmeList.FormattingEnabled = true;
            this.algoritmeList.Items.AddRange(new object[] {
            "Random",
            "Round Robin"});
            this.algoritmeList.Location = new System.Drawing.Point(3, 16);
            this.algoritmeList.Name = "algoritmeList";
            this.algoritmeList.Size = new System.Drawing.Size(167, 21);
            this.algoritmeList.TabIndex = 4;
            this.algoritmeList.SelectedIndexChanged += new System.EventHandler(this.algoritmeList_SelectedIndexChanged);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.deleteServerBtn, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.addServerBtn, 0, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 551);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(173, 32);
            this.tableLayoutPanel5.TabIndex = 8;
            // 
            // deleteServerBtn
            // 
            this.deleteServerBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deleteServerBtn.Enabled = false;
            this.deleteServerBtn.Location = new System.Drawing.Point(89, 3);
            this.deleteServerBtn.Name = "deleteServerBtn";
            this.deleteServerBtn.Size = new System.Drawing.Size(81, 26);
            this.deleteServerBtn.TabIndex = 1;
            this.deleteServerBtn.Text = "Delete";
            this.deleteServerBtn.UseVisualStyleBackColor = true;
            this.deleteServerBtn.Click += new System.EventHandler(this.deleteServerBtn_Click);
            // 
            // addServerBtn
            // 
            this.addServerBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addServerBtn.Location = new System.Drawing.Point(3, 3);
            this.addServerBtn.Name = "addServerBtn";
            this.addServerBtn.Size = new System.Drawing.Size(80, 26);
            this.addServerBtn.TabIndex = 0;
            this.addServerBtn.Text = "Add";
            this.addServerBtn.UseVisualStyleBackColor = true;
            this.addServerBtn.Click += new System.EventHandler(this.addServerBtn_Click);
            // 
            // LoadBalancerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 592);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "LoadBalancerForm";
            this.Text = "Loadbalancer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LoadBalancerForm_FormClosing);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.portNum)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown portNum;
        private System.Windows.Forms.Button startStopBtn;
        private System.Windows.Forms.ComboBox persistanceList;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox algoritmeList;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListBox logList;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button addServerBtn;
        private System.Windows.Forms.Button deleteServerBtn;
        private System.Windows.Forms.TextBox serverTextBox;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ListBox serverList;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
    }
}

